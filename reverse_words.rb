SEPARATORS = [',', ';', ' ']

def reverse_words(text)
	results = Array.new
	text.split(/(["#{SEPARATORS.join('')}"])/, -1).each do |word|
	  unless word == ' '
			results.push(reverse_word(word))
		else
			results.push(word)
		end		
	end	
	concatenated = ""
	results.each { |w| concatenated << "#{w}" }
	concatenated
end

def reverse_word(word)
	word.split(/\s+/).map{|w|wl=w.length-1;(0..wl).map{|i|w[wl-i]}.join}.join(' ')
end