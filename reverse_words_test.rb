require 'test/unit'
require_relative 'reverse_words'
 
class ReverseWordsTest < Test::Unit::TestCase

	def test_reverse_one_word
	    assert_equal('aloh', reverse_words('hola'))
	end

	def test_reverse_two_words_separated_by_blank
	    assert_equal('0 32 65 98', reverse_words('0 23 56 89'))
	end

	def test_reverse_two_words_with_separators_at_the_end
	    assert_equal('0,32,65,98;;;;', reverse_words('0,23,56,89;;;;'))
	end

	def test_reverse_two_words_with_separators_at_the_beginning
	    assert_equal('    0,32,65,98', reverse_words('    0,23,56,89'))
	end

	def test_reverse_two_words_with_more_than_one_separators_in_the_middle
	    assert_equal('0,32;,,65,98', reverse_words('0,23;,,56,89'))
	end

end