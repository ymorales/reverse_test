# How to use #

## Clone the repo
```
$ git clone https://ymorales@bitbucket.org/ymorales/reverse_test.git
```

## Run unit tests
```
$ cd reverse_test 
$ ruby reverse_words_test.rb
```
